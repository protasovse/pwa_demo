module.exports = {
  transpileDependencies: [
    'vuetify',
  ],

  devServer: {
    disableHostCheck: true,
  },

  pwa: {
    name: 'pwa_demo',
  },

  pluginOptions: {
    i18n: {
      locale: 'ru',
      fallbackLocale: 'ru',
      localeDir: 'locales',
      enableInSFC: false,
    },
    moment: {
      locales: [
        'ru',
        'en',
      ],
    },
  },
};
