import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
  },
  {
    path: '/phonebook',
    name: 'Phonebook',
    component: () => import('../views/Phonebook.vue'),
  },
  {
    path: '/messenger',
    name: 'Messenger',
    component: () => import('../views/Messenger.vue'),
  },
  {
    path: '/messenger/contacts',
    name: 'MessengerContacts',
    component: () => import('../views/MessengerContacts.vue'),
  },
  {
    path: '/messenger/thread/:id',
    component: () => import('../views/MessengerThread.vue'),
    props: true,
  },
  {
    path: '/profile/:username',
    props: true,
    name: 'Profile',
    component: () => import('../views/Profile.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
