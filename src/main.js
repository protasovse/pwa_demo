import Vue from 'vue';
import VueBus from 'vue-bus';
import VueNativeSock from 'vue-native-websocket';
import moment from 'moment';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
// import store from './store';
import vuetify from './plugins/vuetify';
import { createProvider } from './plugins/vue-apollo';
import i18n from './plugins/i18n';

Vue.config.productionTip = false;
Vue.prototype.moment = moment;
Vue.prototype.moment.locale('ru');

// Native websocket implementation (https://github.com/nathantsoi/vue-native-websocket)
Vue.use(VueNativeSock, process.env.VUE_APP_WEBSOCKET, { format: 'json', connectManually: true });

Vue.use(VueBus);

new Vue({
  router,
  // store,
  vuetify,
  apolloProvider: createProvider(),
  i18n,
  render: (h) => h(App),
}).$mount('#app');
